<?php
declare (strict_types=1);

namespace plugin\worker;

use plugin\worker\command\Worker;
use think\admin\Plugin;

class Service extends Plugin
{
    /**
     * 定义插件名称
     * @var string
     */
    protected $appName = '网络服务';

    /**
     * 定义安装包名
     * @var string
     */
    protected $package = 'npro/worker';

    public function register()
    {
        $this->commands(['npro:worker' => Worker::class]);
    }

    public static function menu(): array
    {
        return [];
    }
}